package io.swagger.client.api;

import com.google.gson.reflect.TypeToken;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.swagger.client.ApiCallback;
import io.swagger.client.ApiException;
import io.swagger.client.ApiResponse;
import io.swagger.client.Pair;
import io.swagger.client.ProgressRequestBody;
import io.swagger.client.ProgressResponseBody;
import io.swagger.client.model.ImpAuthBody;
import io.swagger.client.model.ImpAuthResponse;
import io.swagger.client.model.LikeTodayCountResponse;
import io.swagger.client.model.Login;
import io.swagger.client.model.LoginResponse;
import io.swagger.client.model.Photo;
import io.swagger.client.model.Posting;
import io.swagger.client.model.ProfileRegister;
import io.swagger.client.model.UserRegister;
import io.swagger.client.model.UserRegisterResponse;

public class RKApi extends ApiApi {

    public void setJwt(String jwt) {
        this.apiClient.setJwt(jwt);
    }


    /**
     * Build call for apiProfilesCreate
     * @param data  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiProfilesCreateCall(ProfileRegister data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener, File representativePhoto1, File representativePhoto2, File representativePhoto3) throws ApiException {
        ProfileRegister profileRegister = new ProfileRegister();
        Object localVarPostBody = profileRegister;

        // create path and map variables
        String localVarPath = "/api/profiles/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        localVarFormParams.put("nickname", data.getNickname());
        if (representativePhoto1 != null)
            localVarFormParams.put("representative_photo1", representativePhoto1);
        if (representativePhoto2 != null)
            localVarFormParams.put("representative_photo2", representativePhoto2);
        if (representativePhoto3 != null)
            localVarFormParams.put("representative_photo3", representativePhoto3);

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiProfilesCreateValidateBeforeCall(ProfileRegister data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener, File representativePhoto1, File representativePhoto2, File representativePhoto3) throws ApiException {

        // verify the required parameter 'data' is set
        if (data == null) {
            throw new ApiException("Missing the required parameter 'data' when calling apiProfilesCreate(Async)");
        }


        com.squareup.okhttp.Call call = apiProfilesCreateCall(data, progressListener, progressRequestListener, representativePhoto1, representativePhoto2, representativePhoto3);
        return call;





    }

    /**
     * 프로필을 생성한다.
     *     Return Profile Instance
     * @param data  (required)
     * @return ProfileRegister
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ProfileRegister apiProfilesCreate(ProfileRegister data, File representativePhoto1, File representativePhoto2, File representativePhoto3) throws ApiException {
        ApiResponse<ProfileRegister> resp = apiProfilesCreateWithHttpInfo(data, representativePhoto1, representativePhoto2, representativePhoto3);
        return resp.getData();
    }

    /**
     * 프로필을 생성한다.
     *     Return Profile Instance
     * @param data  (required)
     * @return ApiResponse&lt;ProfileRegister&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<ProfileRegister> apiProfilesCreateWithHttpInfo(ProfileRegister data, File representativePhoto1, File representativePhoto2, File representativePhoto3) throws ApiException {
        com.squareup.okhttp.Call call = apiProfilesCreateValidateBeforeCall(data, null, null, representativePhoto1, representativePhoto2, representativePhoto3);
        Type localVarReturnType = new TypeToken<ProfileRegister>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 프로필을 생성한다. (asynchronously)
     *     Return Profile Instance
     * @param data  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiProfilesCreateAsync(ProfileRegister data, final ApiCallback<ProfileRegister> callback, File representativePhoto1, File representativePhoto2, File representativePhoto3) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiProfilesCreateValidateBeforeCall(data, progressListener, progressRequestListener, representativePhoto1, representativePhoto2, representativePhoto3);
        Type localVarReturnType = new TypeToken<ProfileRegister>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    public com.squareup.okhttp.Call apiPhotosCreateCall(File image, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/photos/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();
        if (image != null)
            localVarFormParams.put("image", image);

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "multipart/form-data"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiPhotosCreateValidateBeforeCall(File image, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        com.squareup.okhttp.Call call = apiPhotosCreateCall(image, progressListener, progressRequestListener);
        return call;
    }

    /**
     * 이미지(사진)을 업로드 한다.
     * Returns 0
     * @param image  (required)
     * @return Photo
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Photo apiPhotosCreate(File image) throws ApiException {
        ApiResponse<Photo> resp = apiPhotosCreateWithHttpInfo(image);
        return resp.getData();
    }

    public ApiResponse<Photo> apiPhotosCreateWithHttpInfo(File image) throws ApiException {
        com.squareup.okhttp.Call call = apiPhotosCreateValidateBeforeCall(image, null, null);
        Type localVarReturnType = new TypeToken<Photo>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 이미지(사진)을 업로드 한다. (asynchronously)
     * Returns 0
     * @param image  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiPhotosCreateAsync(File image, final ApiCallback<Photo> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiPhotosCreateValidateBeforeCall(image, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Photo>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }


    /**
     * Build call for apiAppealPostingsCreate
     * @param data  (required)
     * @param files
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiAppealPostingsCreateCall(Posting data, File[] files, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = data;

        // create path and map variables
        String localVarPath = "/api/appeal/postings/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        if (files != null) {
            localVarFormParams.put("files", files);
        }


        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "multipart/form-data"
        };

        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiAppealPostingsCreateValidateBeforeCall(Posting data, File[] files, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'data' is set
        if (data == null) {
            throw new ApiException("Missing the required parameter 'data' when calling apiAppealPostingsCreate(Async)");
        }


        com.squareup.okhttp.Call call = apiAppealPostingsCreateCall(data, files, progressListener, progressRequestListener);
        return call;





    }

    /**
     * 어필을 생성한다.
     *     Return Appeal Instance
     * @param data  (required)
     * @param files
     * @return Posting
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public Posting apiAppealPostingsCreate(Posting data, File[] files) throws ApiException {
        ApiResponse<Posting> resp = apiAppealPostingsCreateWithHttpInfo(data, files);
        return resp.getData();
    }

    /**
     * 어필을 생성한다.
     *     Return Appeal Instance
     * @param data  (required)
     * @param files
     * @return ApiResponse&lt;Posting&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<Posting> apiAppealPostingsCreateWithHttpInfo(Posting data, File[] files) throws ApiException {
        com.squareup.okhttp.Call call = apiAppealPostingsCreateValidateBeforeCall(data, files, null, null);
        Type localVarReturnType = new TypeToken<Posting>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 어필을 생성한다. (asynchronously)
     *     Return Appeal Instance
     * @param data  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiAppealPostingsCreateAsync(Posting data, File[] files, final ApiCallback<Posting> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiAppealPostingsCreateValidateBeforeCall(data, files, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Posting>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }


    /**
     * Build call for apiMembersLoginCreate
     * @param data  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiMembersLoginCreateCall(Login data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = data;

        // create path and map variables
        String localVarPath = "/api/members/login/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiMembersLoginCreateValidateBeforeCall(Login data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'data' is set
        if (data == null) {
            throw new ApiException("Missing the required parameter 'data' when calling apiMembersLoginCreate(Async)");
        }


        com.squareup.okhttp.Call call = apiMembersLoginCreateCall(data, progressListener, progressRequestListener);
        return call;





    }

    public LoginResponse apiMembersLoginCreate(Login data, boolean isCustom) throws ApiException {
        ApiResponse<LoginResponse> resp = apiMembersLoginCreateWithHttpInfo(data, isCustom);
        return resp.getData();
    }

    public ApiResponse<LoginResponse> apiMembersLoginCreateWithHttpInfo(Login data, boolean isCustom) throws ApiException {
        com.squareup.okhttp.Call call = apiMembersLoginCreateValidateBeforeCall(data, null, null);
        Type localVarReturnType = new TypeToken<LoginResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * Check the credentials and return the REST Token if the credentials are valid and authenticated. Calls Django Auth login method to register User ID in Django session framework  Accept the following POST parameters: username, password Return the REST Framework Token Object&#39;s key.
     * @param data  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiMembersLoginCreateAsync(Login data, final ApiCallback<Login> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiMembersLoginCreateValidateBeforeCall(data, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<Login>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }


    /**
     * Build call for apiMembersSetImpAuthCreate
     *
     * @param data
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiMembersSetImpAuthCreateCall(ImpAuthBody data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = data;

        // create path and map variables
        String localVarPath = "/api/members/set-imp-auth/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiMembersSetImpAuthCreateValidateBeforeCall(ImpAuthBody data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        com.squareup.okhttp.Call call = apiMembersSetImpAuthCreateCall(data, progressListener, progressRequestListener);
        return call;





    }

    /**
     * 아임포트 인증
     * 유저가 아임포트 인증을 한 적이 있는지 판단 후 없으면 생성 존재 하면 진행 Return 생성-201, 존재-200  Parameters  import_uid - string  / test - string(default&#x3D;None, test&#x3D;on) Response    {detail:msg - string , hash:hash value - seting}
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @param data
     */
    public ImpAuthResponse apiMembersSetImpAuthCreate(ImpAuthBody data) throws ApiException {

        ApiResponse<ImpAuthResponse> resp = apiMembersSetImpAuthCreateWithHttpInfo(data);
        return resp.getData();

    }

    /**
     * 아임포트 인증
     * 유저가 아임포트 인증을 한 적이 있는지 판단 후 없으면 생성 존재 하면 진행 Return 생성-201, 존재-200  Parameters  import_uid - string  / test - string(default&#x3D;None, test&#x3D;on) Response    {detail:msg - string , hash:hash value - seting}
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     * @param data
     */
    public ApiResponse<ImpAuthResponse> apiMembersSetImpAuthCreateWithHttpInfo(ImpAuthBody data) throws ApiException {
        com.squareup.okhttp.Call call = apiMembersSetImpAuthCreateValidateBeforeCall(data, null, null);
        Type localVarReturnType = new TypeToken<ImpAuthResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     * 아임포트 인증 (asynchronously)
     * 유저가 아임포트 인증을 한 적이 있는지 판단 후 없으면 생성 존재 하면 진행 Return 생성-201, 존재-200  Parameters  import_uid - string  / test - string(default&#x3D;None, test&#x3D;on) Response    {detail:msg - string , hash:hash value - seting}
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiMembersSetImpAuthCreateAsync(ImpAuthBody data, final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiMembersSetImpAuthCreateValidateBeforeCall(data, progressListener, progressRequestListener);
        apiClient.executeAsync(call, callback);
        return call;
    }


    /**
     * Build call for apiMembersRegistrationCreate
     * @param data  (required)
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiMembersRegistrationCreateCall(UserRegister data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = data;

        // create path and map variables
        String localVarPath = "/api/members/registration/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(com.squareup.okhttp.Interceptor.Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "POST", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiMembersRegistrationCreateValidateBeforeCall(UserRegister data, final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {

        // verify the required parameter 'data' is set
        if (data == null) {
            throw new ApiException("Missing the required parameter 'data' when calling apiMembersRegistrationCreate(Async)");
        }


        com.squareup.okhttp.Call call = apiMembersRegistrationCreateCall(data, progressListener, progressRequestListener);
        return call;





    }

    public UserRegisterResponse apiMembersRegistrationCreate(UserRegister data, Boolean isCustom) throws ApiException {
        ApiResponse<UserRegisterResponse> resp = apiMembersRegistrationCreateWithHttpInfo(data, isCustom);
        return resp.getData();
    }

    public ApiResponse<UserRegisterResponse> apiMembersRegistrationCreateWithHttpInfo(UserRegister data, Boolean isCustom) throws ApiException {
        com.squareup.okhttp.Call call = apiMembersRegistrationCreateValidateBeforeCall(data, null, null);
        Type localVarReturnType = new TypeToken<UserRegisterResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     *  (asynchronously)
     *
     * @param data  (required)
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiMembersRegistrationCreateAsync(UserRegister data, final ApiCallback<UserRegister> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiMembersRegistrationCreateValidateBeforeCall(data, progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<UserRegisterResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }

    /**
     * Build call for apiProfilesLikeTodayCountList
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiProfilesLikeTodayCountListCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/profiles/like/today_count/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiProfilesLikeTodayCountListValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {


        com.squareup.okhttp.Call call = apiProfilesLikeTodayCountListCall(progressListener, progressRequestListener);
        return call;





    }

    /**
     *
     * 오늘 좋아요 가능한 횟수를 가져 온다.
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public LikeTodayCountResponse apiProfilesLikeTodayCountList(Boolean isCustom) throws ApiException {
        return apiProfilesLikeTodayCountListWithHttpInfo(isCustom).getData();
    }

    /**
     *
     * 오늘 좋아요 가능한 횟수를 가져 온다.
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<LikeTodayCountResponse> apiProfilesLikeTodayCountListWithHttpInfo(Boolean isCustom) throws ApiException {
        com.squareup.okhttp.Call call = apiProfilesLikeTodayCountListValidateBeforeCall(null, null);
        Type localVarReturnType = new TypeToken<LikeTodayCountResponse>(){}.getType();
        return apiClient.execute(call, localVarReturnType);
    }

    /**
     *  (asynchronously)
     * 오늘 좋아요 가능한 횟수를 가져 온다.
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiProfilesLikeTodayCountListAsync(final ApiCallback<LikeTodayCountResponse> callback, Boolean isCustom) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiProfilesLikeTodayCountListValidateBeforeCall(progressListener, progressRequestListener);
        Type localVarReturnType = new TypeToken<LikeTodayCountResponse>(){}.getType();
        apiClient.executeAsync(call, localVarReturnType, callback);
        return call;
    }


    /**
     * Build call for apiProfilesChoicesList
     * @param progressListener Progress listener
     * @param progressRequestListener Progress request listener
     * @return Call to execute
     * @throws ApiException If fail to serialize the request body object
     */
    public com.squareup.okhttp.Call apiProfilesChoicesListCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {
        Object localVarPostBody = null;

        // create path and map variables
        String localVarPath = "/api/profiles/choices/";

        List<Pair> localVarQueryParams = new ArrayList<Pair>();

        Map<String, String> localVarHeaderParams = new HashMap<String, String>();

        Map<String, Object> localVarFormParams = new HashMap<String, Object>();

        final String[] localVarAccepts = {
                "application/json"
        };
        final String localVarAccept = apiClient.selectHeaderAccept(localVarAccepts);
        if (localVarAccept != null) localVarHeaderParams.put("Accept", localVarAccept);

        final String[] localVarContentTypes = {
                "application/json"
        };
        final String localVarContentType = apiClient.selectHeaderContentType(localVarContentTypes);
        localVarHeaderParams.put("Content-Type", localVarContentType);

        if(progressListener != null) {
            apiClient.getHttpClient().networkInterceptors().add(new com.squareup.okhttp.Interceptor() {
                @Override
                public com.squareup.okhttp.Response intercept(Chain chain) throws IOException {
                    com.squareup.okhttp.Response originalResponse = chain.proceed(chain.request());
                    return originalResponse.newBuilder()
                            .body(new ProgressResponseBody(originalResponse.body(), progressListener))
                            .build();
                }
            });
        }

        String[] localVarAuthNames = new String[] { "api_key" };
        return apiClient.buildCall(localVarPath, "GET", localVarQueryParams, localVarPostBody, localVarHeaderParams, localVarFormParams, localVarAuthNames, progressRequestListener);
    }

    @SuppressWarnings("rawtypes")
    private com.squareup.okhttp.Call apiProfilesChoicesListValidateBeforeCall(final ProgressResponseBody.ProgressListener progressListener, final ProgressRequestBody.ProgressRequestListener progressRequestListener) throws ApiException {


        com.squareup.okhttp.Call call = apiProfilesChoicesListCall(progressListener, progressRequestListener);
        return call;





    }

    /**
     *
     * profiles/model_choices.py에 있는 Type을 보냄. BaseType 제외 return AllType Dict
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public String apiProfilesChoicesList(boolean isCustom) throws ApiException {
        return apiProfilesChoicesListWithHttpInfo(isCustom).getData();
    }

    /**
     *
     * profiles/model_choices.py에 있는 Type을 보냄. BaseType 제외 return AllType Dict
     * @return ApiResponse&lt;Void&gt;
     * @throws ApiException If fail to call the API, e.g. server error or cannot deserialize the response body
     */
    public ApiResponse<String> apiProfilesChoicesListWithHttpInfo(boolean isCustom) throws ApiException {
        com.squareup.okhttp.Call call = apiProfilesChoicesListValidateBeforeCall(null, null);
        return apiClient.execute(call, String.class);
    }

    /**
     *  (asynchronously)
     * profiles/model_choices.py에 있는 Type을 보냄. BaseType 제외 return AllType Dict
     * @param callback The callback to be executed when the API call finishes
     * @return The request call
     * @throws ApiException If fail to process the API call, e.g. serializing the request body object
     */
    public com.squareup.okhttp.Call apiProfilesChoicesListAsync(final ApiCallback<Void> callback) throws ApiException {

        ProgressResponseBody.ProgressListener progressListener = null;
        ProgressRequestBody.ProgressRequestListener progressRequestListener = null;

        if (callback != null) {
            progressListener = new ProgressResponseBody.ProgressListener() {
                @Override
                public void update(long bytesRead, long contentLength, boolean done) {
                    callback.onDownloadProgress(bytesRead, contentLength, done);
                }
            };

            progressRequestListener = new ProgressRequestBody.ProgressRequestListener() {
                @Override
                public void onRequestProgress(long bytesWritten, long contentLength, boolean done) {
                    callback.onUploadProgress(bytesWritten, contentLength, done);
                }
            };
        }

        com.squareup.okhttp.Call call = apiProfilesChoicesListValidateBeforeCall(progressListener, progressRequestListener);
        apiClient.executeAsync(call, String.class, callback);
        return call;
    }
}
