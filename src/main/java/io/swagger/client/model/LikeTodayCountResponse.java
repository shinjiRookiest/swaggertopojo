
package io.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class LikeTodayCountResponse {

    @SerializedName("today_count")
    private int mToday_count;

    public int getTodayCount() {
        return mToday_count;
    }

    public void setTodayCount(int today_count) {
        mToday_count = today_count;
    }
}
