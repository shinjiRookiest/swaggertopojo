
package io.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class User {

    @SerializedName("created")
    private String mCreated;
    @SerializedName("email")
    private String mEmail;
    @SerializedName("is_agree_email")
    private Boolean mIsAgreeEmail;
    @SerializedName("is_agree_privacy")
    private Boolean mIsAgreePrivacy;
    @SerializedName("is_agree_service")
    private Boolean mIsAgreeService;
    @SerializedName("is_agree_sms")
    private Boolean mIsAgreeSms;
    @SerializedName("pk")
    private Long mPk;
    @SerializedName("profile")
    private Object mProfile;

    public String getCreated() {
        return mCreated;
    }

    public void setCreated(String created) {
        mCreated = created;
    }

    public String getEmail() {
        return mEmail;
    }

    public void setEmail(String email) {
        mEmail = email;
    }

    public Boolean getIsAgreeEmail() {
        return mIsAgreeEmail;
    }

    public void setIsAgreeEmail(Boolean isAgreeEmail) {
        mIsAgreeEmail = isAgreeEmail;
    }

    public Boolean getIsAgreePrivacy() {
        return mIsAgreePrivacy;
    }

    public void setIsAgreePrivacy(Boolean isAgreePrivacy) {
        mIsAgreePrivacy = isAgreePrivacy;
    }

    public Boolean getIsAgreeService() {
        return mIsAgreeService;
    }

    public void setIsAgreeService(Boolean isAgreeService) {
        mIsAgreeService = isAgreeService;
    }

    public Boolean getIsAgreeSms() {
        return mIsAgreeSms;
    }

    public void setIsAgreeSms(Boolean isAgreeSms) {
        mIsAgreeSms = isAgreeSms;
    }

    public Long getPk() {
        return mPk;
    }

    public void setPk(Long pk) {
        mPk = pk;
    }

    public Object getProfile() {
        return mProfile;
    }

    public void setProfile(Object profile) {
        mProfile = profile;
    }

}
