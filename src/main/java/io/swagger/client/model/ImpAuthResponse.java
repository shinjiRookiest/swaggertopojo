
package io.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ImpAuthResponse {

    @SerializedName("detail")
    private String mDetail;
    @SerializedName("hash")
    private String mHash;
    @SerializedName("step")
    private String mStep;

    public String getDetail() {
        return mDetail;
    }

    public void setDetail(String detail) {
        mDetail = detail;
    }

    public String getHash() {
        return mHash;
    }

    public void setHash(String hash) {
        mHash = hash;
    }

    public String getStep() {
        return mStep;
    }

    public void setStep(String step) {
        mStep = step;
    }

}
