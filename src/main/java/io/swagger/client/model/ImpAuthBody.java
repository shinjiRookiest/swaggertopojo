
package io.swagger.client.model;

import com.google.gson.annotations.SerializedName;

import javax.annotation.Generated;

@Generated("net.hexar.json2pojo")
@SuppressWarnings("unused")
public class ImpAuthBody {

    @SerializedName("import_uid")
    private String mImportUid;
    @SerializedName("test")
    private String mTest;

    public String getImportUid() {
        return mImportUid;
    }

    public void setImportUid(String importUid) {
        mImportUid = importUid;
    }

    public String getTest() {
        return mTest;
    }

    public void setTest(String test) {
        mTest = test;
    }

}
