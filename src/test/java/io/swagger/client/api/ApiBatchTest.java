package io.swagger.client.api;

import org.joda.time.LocalDate;
import org.junit.Ignore;
import org.junit.Test;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.charset.Charset;
import java.nio.file.DirectoryStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import javax.imageio.ImageIO;

import io.swagger.client.ApiException;
import io.swagger.client.JCodecPNGtoMP4;
import io.swagger.client.model.ImpAuthBody;
import io.swagger.client.model.ImpAuthResponse;
import io.swagger.client.model.InlineResponse200;
import io.swagger.client.model.InlineResponse2001;
import io.swagger.client.model.InlineResponse2002;
import io.swagger.client.model.InlineResponse2004;
import io.swagger.client.model.InlineResponse2006;
import io.swagger.client.model.InlineResponse2007;
import io.swagger.client.model.Like;
import io.swagger.client.model.LikeTodayCountResponse;
import io.swagger.client.model.Login;
import io.swagger.client.model.LoginResponse;
import io.swagger.client.model.Photo;
import io.swagger.client.model.Posting;
import io.swagger.client.model.PostingFile;
import io.swagger.client.model.Profile;
import io.swagger.client.model.ProfileInfo;
import io.swagger.client.model.ProfileRegister;
import io.swagger.client.model.User;
import io.swagger.client.model.UserRegister;
import io.swagger.client.model.UserRegisterResponse;

@Ignore
public class ApiBatchTest {

    private static RKApi api = new RKApi();

    public LoginResponse apiMembersLoginCreateTest(String email, String pass) throws ApiException {
        Login data = new Login();
        data.setEmail(email);
        data.setPassword(pass);
        return api.apiMembersLoginCreate(data, true);
    }

    public Posting apiAppealPostingsCreateTest(Posting data, File[] files) throws ApiException {
        return  api.apiAppealPostingsCreate(data, files);
    }

    public InlineResponse200 apiAppealCommentsListTest(Integer limit, Integer offset) throws ApiException {
        return api.apiAppealCommentsList(limit, offset);
    }

    public Like apiProfilesLikeCreateTest(User author, User receiver) throws ApiException {
        Like data = new Like();
        data.setReceiver((int) (long) receiver.getPk());
        return api.apiProfilesLikeCreate(data);
    }

    public InlineResponse2001 apiAppealLikesListTest(Integer limit, Integer offset) throws ApiException {
        return api.apiAppealLikesList(limit, offset);
    }

    public ImpAuthResponse apiMembersSetImpAuthCreateTest() throws ApiException {

        byte[] array = new byte[7]; // length is bounded by 7
        new Random().nextBytes(array);
        String generatedString = new String(array, Charset.forName("UTF-8"));

        ImpAuthBody data = new ImpAuthBody();
        data.setImportUid(generatedString);
        data.setTest("on");

        return api.apiMembersSetImpAuthCreate(data);
    }

    public UserRegisterResponse apiMembersRegistrationCreateTest(String hash) throws ApiException {

        UserRegister data = new UserRegister();
        data.setIsAgreeService(true);
        data.setIsAgreePrivacy(true);
        data.setIsAgreeSms(true);
        data.setIsAgreeEmail(true);
        data.setEmail( makeRandomString(10) + "@test.test");
        data.setPassword1("11test11");
        data.setPassword2("11test11");
        data.setUidHash(hash);

        return api.apiMembersRegistrationCreate(data, true);
    }

    public ProfileRegister apiProfilesCreateTest() throws ApiException {
        ProfileRegister data = new ProfileRegister();
        String nickname = makeRandomString(8);
        data.nickname(nickname);
        data.setGender(ProfileRegister.GenderEnum.N);
        data.setBirthday(LocalDate.parse("2019-03-13"));
        data.setLength(0);
        data.setBody(ProfileRegister.BodyEnum._1);
        data.setSpec(ProfileRegister.SpecEnum._1);
        data.setCharacter(ProfileRegister.CharacterEnum._1);
        data.setReligion(ProfileRegister.ReligionEnum._1);
        data.setDrinking(ProfileRegister.DrinkingEnum._1);
        data.smoking(ProfileRegister.SmokingEnum._1);
        data.setIntroduction("test instroduction");
        data.setAttractivePoint(Arrays.asList(ProfileRegister.AttractivePointEnum._1, ProfileRegister.AttractivePointEnum._2));
        data.setInterest(Arrays.asList(ProfileRegister.InterestEnum._1, ProfileRegister.InterestEnum._2));
        data.setPreferType(Arrays.asList(ProfileRegister.PreferTypeEnum._1, ProfileRegister.PreferTypeEnum._2));
        data.setDateStyle(Arrays.asList(ProfileRegister.DateStyleEnum._1, ProfileRegister.DateStyleEnum._2));
        data.setLatitude("latitude");
        data.longitude("longitude");
        data.setCity("city");
        data.setPhonenumber("01000000000");
        data.setDeviceId("device_id");

        String representativePhoto1 = makeImageByText(nickname, "representativePhoto1");
        String representativePhoto2 = makeImageByText(nickname, "representativePhoto2");
        String representativePhoto3 = makeImageByText(nickname, "representativePhoto3");
        File file1 = new File(representativePhoto1);
        File file2 = new File(representativePhoto2);
        File file3 = new File(representativePhoto3);

        return api.apiProfilesCreate(data, file1, file2, file3);
    }

    public Photo apiPhotosCreateTest(String nick, String text) throws ApiException {

        String addedPhoto = makeImageByText(nick, text);
        File image = new File(addedPhoto);
        return api.apiPhotosCreate(image);
    }

    public InlineResponse2006 apiPhotosListTest() throws ApiException {
        Integer limit = 99;
        Integer offset = 0;
        return api.apiPhotosList(limit, offset);
    }

    public InlineResponse2004 apiMatchesTodayListTest(Integer limit, Integer offset) throws ApiException {
        return api.apiMatchesTodayList(limit, offset);
    }

    public ProfileInfo apiProfilesReadTest(Integer id) throws ApiException {
        return api.apiProfilesRead(id);
    }

    public InlineResponse2007 apiProfilesListTest(Integer limit, Integer offset) throws ApiException {
        return api.apiProfilesList(limit, offset);
    }

    public InlineResponse2002 apiAppealPostingsListTest(Integer limit, Integer offset) throws ApiException {
        return api.apiAppealPostingsList(limit, offset);
    }

    public LikeTodayCountResponse apiProfilesLikeTodayCountListTest() throws ApiException {
        return api.apiProfilesLikeTodayCountList(true);
    }

    @Test
    public void batchTest() throws Exception {

        ImpAuthResponse impAuthResponse = apiMembersSetImpAuthCreateTest();
        UserRegisterResponse userRegisterResponse = apiMembersRegistrationCreateTest(impAuthResponse.getHash());

        api.setJwt(userRegisterResponse.getToken());

        ProfileRegister profileRegister = apiProfilesCreateTest();
        String nick = profileRegister.getNickname();
        Photo photo1 = apiPhotosCreateTest(nick, "addedPhoto1");
        Photo photo2 = apiPhotosCreateTest(nick, "addedPhoto2");
        InlineResponse2006 inlineResponse2006 = apiPhotosListTest();
        String video_path = JCodecPNGtoMP4.makeVideo(nick);

        // posting 나중에 하기
//        Posting data = new Posting();
//        data.setBody("test posting");
//        Path directoryPath = Paths.get(new File("pngs\\" + nick).toURI());
//        DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, "*." + "png");
//        List<PostingFile> filesList = new ArrayList<PostingFile>();
//        for (Path path : stream) {
//            PostingFile pf = new PostingFile();
//            String ps = path.toString();
//            String as = path.getFileName().toFile().getAbsolutePath().toString();
//            pf.setFile(as);
//            filesList.add(pf);
//        }
//        data.setFiles(filesList);
//        Posting post = apiAppealPostingsCreateTest(data);



//        apiMatchesTodayReadTest();
//        apiMembersLoginCreateTest();
//        apiAppealPostingsCreateTest();
//        apiAppealCommentsListTest();
//        apiProfilesLikeCreateTest();

    }

    @Test
    public void batchTestLogin() throws ApiException, IOException {
        LoginResponse loginResponse = apiMembersLoginCreateTest("browndavid@yahoo.com", "1punch2PUNCH");

        api.setJwt(loginResponse.getToken());

        String resp = api.apiProfilesChoicesList(true);

        InlineResponse2004 profiles = apiMatchesTodayListTest(6, 0);

        for(int i = 0; i<profiles.getResults().size() ; i++){
            Profile pr = profiles.getResults().get(i);
            ProfileInfo pi = apiProfilesReadTest(pr.getPk());
            System.out.println(pi.getNickname());
        }

        InlineResponse2007 inlineResponse2007 = apiProfilesListTest(15, 0);
            List<ProfileInfo> tour = inlineResponse2007.getResults();

        for(int i = 0; i<tour.size() ; i++){
            ProfileInfo pi = tour.get(i);
            System.out.println(pi.getNickname());
        }

        InlineResponse2002 inlineResponse2002 = apiAppealPostingsListTest(18, 0);

        List<Posting> postings = inlineResponse2002.getResults();

        for(int i = 0; i < postings.size(); i++ ) {
            Posting po = postings.get(i);
            System.out.print(po.getBody());
        }

        LikeTodayCountResponse todayCountResponse = apiProfilesLikeTodayCountListTest();
        System.out.print(todayCountResponse.getTodayCount());




//        Posting data = new Posting();
//        data.setBody("test posting");
//        Path directoryPath = Paths.get(new File("pngs\\" + "PmKfBFSt").toURI());
//        DirectoryStream<Path> stream = Files.newDirectoryStream(directoryPath, "*." + "png");
//        List<File> filesList = new ArrayList<File>();
//
//        for (Path path : stream) {
//            filesList.add(path.toFile());
//        }
//        File videoFile = new File("mp4s\\" + "PmKfBFSt.mp4" );
//        filesList.add(videoFile);
//
//        File[] files = filesList.toArray(new File[filesList.size()]);
//
//        filesList.toArray(files);
//
//        Posting post = apiAppealPostingsCreateTest(data, files);
//
//        System.out.println(post.toString());




    }
//
//    public void makeVideoFromImages(String imagePath){
//        AWTSequenceEncoder encoder = AWTSequenceEncoder.createSequenceEncoder(new File(imagePath), 25); // 25 fps
//        for (BufferedImage image : getImagesFromSomewhere()) {
//            encoder.encodeImage(image);
//        }
//        encoder.finish();
//    }

    public String makeRandomString(int length) {


        char[] chars = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRST".toCharArray();

        StringBuilder sb = new StringBuilder();
        Random random = new Random();
        for (int i = 0; i < length; i++) {
            char c = chars[random.nextInt(chars.length)];
            sb.append(c);
        }
        final String generatedString = sb.toString();
        return generatedString;
    }

    public String makeImageByText(String nick, String file_name){

        String folder_path = "pngs\\";

        File theDir = new File(folder_path + nick);

        if (!theDir.exists()) {
            System.out.println("creating directory: " + theDir.getName());
            try{
                theDir.mkdir();
            }
            catch(SecurityException se){}
        }

        folder_path = folder_path + nick + "\\";

        Random random = new Random();
        BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();
        Font font = new Font("Arial", Font.PLAIN, 24);
        g2d.setFont(font);
        FontMetrics fm = g2d.getFontMetrics();
        int width = 480;
        int height = 600;
        g2d.dispose();

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_RGB);
        g2d = img.createGraphics();
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);
        g2d.setFont(font);
        fm = g2d.getFontMetrics();

        int red = random.nextInt(128) + 128;
        int green = random.nextInt(128) + 128;
        int blue = random.nextInt(128) + 128;
        Color randomColour = new Color(red,green,blue);

        g2d.setColor(randomColour);
        g2d.fillRect(0, 0, width, height);
        g2d.setColor(Color.BLACK);
        g2d.drawString(nick + " : " +file_name, 0, fm.getAscent());
        g2d.dispose();

        try {
            File file = new File(folder_path + file_name + ".png");
            ImageIO.write(img, "png", file);
            System.out.println(file.getAbsolutePath());
            return file.getAbsolutePath();
        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
    }
}
